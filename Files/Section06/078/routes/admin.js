const path = require('path'); // use path.js helper for root dir

const express = require('express'); // express.js

const rootDir = require('../util/path'); // allows us to use rootDir in path

const router = express.Router(); // allows us to router methods

const products = []; // make an array for products

// /admin/add-product => GET
router.get('/add-product', (req, res, next) => {
  res.sendFile(path.join(rootDir, 'views', 'add-product.html'));
});

// /admin/add-product => POST
router.post('/add-product', (req, res, next) => {
  products.push({ title: req.body.title }) // add title object to the array
  res.redirect('/');
});

exports.routes = router; // export the routes
exports.products = products; // export the array
