const path = require('path');

const express = require('express');

const rootDir = require('../util/path');
const adminData = require('./admin'); // gives access to products array

const router = express.Router();

router.get('/', (req, res, next) => {
  const products = adminData.products;
  res.render('shop', {prods: products, docTitle: 'Shop'}); 
  /* will use default templating engine
  will send data to shop.pug
  will send products as 'prods'
  and 'Shop' as docTitle*/
});

module.exports = router;
