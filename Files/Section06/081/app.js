const path = require('path'); // use path.js helper for root dir

const express = require('express'); // express.js
const bodyParser = require('body-parser'); // middleware, parses requests

const app = express(); // use express

app.set('view engine', 'pug'); // sets pug as our view engine

const adminData = require('./routes/admin'); // admin.js routes
const shopRoutes = require('./routes/shop'); // shop.js routes

app.use(bodyParser.urlencoded({extended: false})); // parses body into key-value pairs
app.use(express.static(path.join(__dirname, 'public')));// use the public folder

app.use('/admin', adminData.routes); // use the routes object from admin.js
app.use(shopRoutes); // use the shop routes next

// send a 404 if none of the above 
app.use((req, res, next) => {
    res.status(404).sendFile(path.join(__dirname, 'views', '404.html'));
});

app.listen(3000); // localhost 3000
