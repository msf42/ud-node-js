// import file system to work with files
const fs = require('fs');

// import path so we use relative paths
const path = require('path');

// now "p" will refer to /data/cart.json
const p = path.join(
  path.dirname(process.mainModule.filename),
  'data',
  'cart.json'
);

// the Cart module used throughout the project
module.exports = class Cart {
  // static, so not called when class is instantiated
  // takes id and price, adds product to cart
  static addProduct(id, productPrice) {
    // Fetch the previous cart
    fs.readFile(p, (err, fileContent) => {
      // new object of products and price
      let cart = { products: [], totalPrice: 0 };
      if (!err) {
        cart = JSON.parse(fileContent);
      }
      // Find index of existing product in cart
      const existingProductIndex = cart.products.findIndex(
        prod => prod.id === id
      );
      // define the existing product
      const existingProduct = cart.products[existingProductIndex];
      let updatedProduct;
      // if there is an existing product
      if (existingProduct) {
        // get the object
        updatedProduct = { ...existingProduct };
        // increase the quantity
        updatedProduct.qty = updatedProduct.qty + 1;
        // get the product list
        cart.products = [...cart.products];
        // update the existing product in the list
        cart.products[existingProductIndex] = updatedProduct;
      } else {
        // if product does not exist in cart, define it at qty of 1
        updatedProduct = { id: id, qty: 1 };
        // add it to the array
        cart.products = [...cart.products, updatedProduct];
      }
      // update price total; coerce productPrice to a num
      cart.totalPrice = cart.totalPrice + +productPrice;
      // write to the JSON
      fs.writeFile(p, JSON.stringify(cart), err => {
        console.log(err);
      });
    });
  }

  // static, so not called when class is instantiated
  static deleteProduct(id, productPrice) {
    // read the file
    fs.readFile(p, (err, fileContent) => {
      if (err) {
        return
      }
      // updatedCart from the file
      const updatedCart = { ...JSON.parse(fileContent) };
      // find the product, return if not found
      const product = updatedCart.products.find(prod => prod.id === id);
      if (!product) {
        return;
      }
      // get quantity from cart
      const productQty = product.qty;
      // update the products in cart, then update price
      updatedCart.products = updatedCart.products.filter(prod => prod.id !== id)
      updatedCart.totalPrice = updatedCart.totalPrice - productPrice * productQty;
      // write new info to JSON
      fs.writeFile(p, JSON.stringify(updatedCart), err => {
        console.log(err)
      })
    })
  }

  // static, so not called when class is instantiated
  // simply reads file and defines cart
  static getCart(cb) {
    fs.readFile(p, (err, fileContent) => {
      const cart = JSON.parse(fileContent);
      if (err) {
        cb(null);
      } else {
        cb(cart);
      }
    })
  }
};
