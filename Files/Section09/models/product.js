// import file system to work with files
const fs = require('fs');

// import path so we use relative paths
const path = require('path');

// import to have access to cart
const Cart = require('./cart');

// now "p" will refer to /data/products.json
const p = path.join(
  path.dirname(process.mainModule.filename),
  'data',
  'products.json'
);
  
// takes in callback
// reads "p", file defined above
// returns parsed file content
const getProductsFromFile = (cb) => {
  fs.readFile(p, (err, fileContent) => {
    if (err) {
      cb([]);
    } else {
      cb(JSON.parse(fileContent));
    }
  })
}

// the Product module used throughout the project
module.exports = class Product {
  // when `new Product` is used, this constructor builds a new Product
  constructor(id, title, imageUrl, description, price) {
    this.id = id;
    this.title = title;
    this.imageUrl = imageUrl;
    this.description = description;
    this.price = price;
  }

  // called when we save a new Product
  save() {
    getProductsFromFile(products => {
      if (this.id) {
        // if product has an id, get the index
        const existingProductIndex = products.findIndex(
          prod => prod.id === this.id
          );
        // then make updatedProducts array starting with old array
        const updatedProducts = [...products];
        updatedProducts[existingProductIndex] = this;
        // now add the new product
        fs.writeFile(p, JSON.stringify(updatedProducts), (err) => {
          console.log(err)
        })
      } else {
        // if product has no id, assign a random one
        this.id = Math.random().toString();
        // add it to products array
        products.push(this);
        // write it to the file
        fs.writeFile(p, JSON.stringify(products), (err) => {
          console.log(err)
        })
      }
    });
  }

  // static, so not called when class is instantiated
  static deleteById(id) {
    // reads the JSON
    getProductsFromFile(products => {
      // locates the product
      const product = products.filter(prod => prod.id === id);
      // makes new updatedProducts without it
      const updatedProducts = products.filter(prod => prod.id !== id);
      // sends new list to JSON
      fs.writeFile(p, JSON.stringify(updatedProducts), err => {
        if (!err) {
          Cart.deleteProduct(id, product.price);
        }
      })
    })
  }

  // static, so not called when class is instantiated
  // gets all products
  static fetchAll(cb) {
    getProductsFromFile(cb);
  }

  // static, so not called when class is instantiated
  // locates and returns the product
  static findById(id, cb) {
    getProductsFromFile(products => {
      const product = products.find(p => p.id === id);
      cb(product);
    })
  }

}