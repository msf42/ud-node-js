// so all paths are relative
const path = require('path');

// import express js
const express = require('express');

// shopController will be used to call methods in controllers/shop
const shopController = require('../controllers/shop');

// import express router
const router = express.Router();

// /products => GET
router.get('/products', shopController.getProducts);

// /products/:productId => GET
router.get('/products/:productId', shopController.getProduct)

// main page ('/') => GET
router.get('/', shopController.getIndex);

// /cart => GET
router.get('/cart', shopController.getCart);

// /cart => POST
router.post('/cart', shopController.postCart);

// /cart-delete-item => POST
router.post('/cart-delete-item', shopController.postCartDeleteProduct);

// /orders => GET
router.get('/orders', shopController.getOrders);

// /checkout => GET
router.get('/checkout', shopController.getCheckout);

module.exports = router;
