// routes/admin.js says what to do when a route is hit
// it calls a function from this file
// the function here says where to go and what to render

// import so we have all Product data
const Product = require('../models/product');

// /admin/add-product => GET
// renders the edit-product page, because they are same page
exports.getAddProduct = (req, res, next) => {
  res.render('admin/edit-product', {
    pageTitle: 'Add Product',
    path: '/admin/add-product',
    editing: false
  });
};

// /admin/add-product => POST
// creates `product` based on the request
exports.postAddProduct = (req, res, next) => {
  const title = req.body.title;
  const imageUrl = req.body.imageUrl;
  const price = req.body.price;
  const description = req.body.description;
  const product = new Product(null, title, imageUrl, description, price);
  product.save();
  res.redirect('/');
};

// /adimin/edit-product => GET
// sets mode to edit, redirects home if not edit mode
// gets product ID from params
// gets product by findById, redirects home if not found
// otherwise renders edit page and passes product info
exports.getEditProduct = (req, res, next) => {
  const editMode = req.query.edit;
  if (!editMode) {
    return res.redirect('/');
  }
  const prodId = req.params.productId;
  Product.findById(prodId, product => {
    if (!product) {
      return res.redirect('/');
    }
    res.render('admin/edit-product', {
      pageTitle: 'Edit Product',
      path: '/admin/edit-product',
      editing: editMode,
      product: product
    });
  });
};

// /admin/edit-product => POST
// gets product info from request
// creates updated product
// saves it and redirects to admin/products
exports.postEditProduct = (req, res, next) => {
  const prodId = req.body.productId;
  const updatedTitle = req.body.title;
  const updatedPrice = req.body.price;
  const updatedImageUrl = req.body.imageUrl;
  const updatedDesc = req.body.description;
  const updatedProduct = new Product(
    prodId,
    updatedTitle,
    updatedImageUrl,
    updatedDesc,
    updatedPrice
  )
  updatedProduct.save();
  res.redirect('/admin/products')
}

// /admin/products => GET
// gets all info from Products module
// passes product info
exports.getProducts = (req, res, next) => {
  Product.fetchAll(products => {
    res.render('admin/products', {
      prods: products,
      pageTitle: 'Admin Products',
      path: '/admin/products'
    });
  });
};

// /admin/delete-product => POST
// gets product ID from request
// deletes from Product module using deleteById
// redirects to /admin/products
exports.postDeleteProduct = (req, res, next) => {
  const prodId = req.body.productId;
  Product.deleteById(prodId);
  res.redirect('/admin/products');
}
