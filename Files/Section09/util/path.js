// path module is for file and directory paths
const path = require('path');

module.exports = path.dirname(process.mainModule.filename);