// location of db functions
const db = require('../util/database')

// import to have access to cart
const Cart = require('./cart');

// the Product module used throughout the project
module.exports = class Product {
  // when `new Product` is used, this constructor builds a new Product
  constructor(id, title, imageUrl, description, price) {
    this.id = id;
    this.title = title;
    this.imageUrl = imageUrl;
    this.description = description;
    this.price = price;
  }

  // called when we save a new Product
  save() {
    return db.execute('INSERT INTO products (title, price, description, imageUrl) VALUES(?, ?, ?, ?)',
    [this.title, this.price, this.description, this.imageUrl])
  }

  // static, so not called when class is instantiated
  static deleteById(id) {

  }

  // static, so not called when class is instantiated
  // gets all products
  static fetchAll() {
    return db.execute('SELECT * FROM products');
  }

  // static, so not called when class is instantiated
  // locates and returns the product
  static findById(id) {
    return db.execute('SELECT * FROM products WHERE products.id = ?', [id])
  }
}