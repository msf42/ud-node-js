// main js file that imports all components
const path = require('path');

const express = require('express');

const bodyParser = require('body-parser');

const errorController = require('./controllers/error');

// allows connection to db pool
const db = require('./util/database');

// everything prefixed with app is using express
const app = express();

// set view engine to ejs
app.set('view engine', 'ejs');
app.set('views', 'views');

// get routes
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');

// import bodyParser and express
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// import routes
app.use('/admin', adminRoutes);
app.use(shopRoutes);

app.use(errorController.get404);

// localhost address
app.listen(3000);
