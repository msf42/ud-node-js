// allows connection to sql database
const mysql = require('mysql2')

// creates a pool of connections so we can have multiple queries, and connect any time we need to
const pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  database: 'node-complete',
  password: 'G.4swjeoj'
})

module.exports = pool.promise();