const express = require('express');

const app = express();

app.use('/users', (req, res, next) => {
    res.send('<h1>The "/users" page has been accessed</h1>');
});

app.use('/', (req, res, next) => {
    res.send('<h1>The "/" page has been accessed</h1>');
});

app.listen(3000);
