const fs = require("fs");

const requestHandler = (req, res) => {
  const url = req.url;
  const method = req.method;

  if (url === "/") {
    res.write("<html>");
    res.write("<head><title>Enter User Info</title></head>");
    res.write("<h1>Welcome!</h1>");
    res.write(
      '<body><form action="/create-user" method="POST"><label>User Name: </label><input type="text" name="user"><button type="submit">Send</button></form></body>'
    );
    res.write("</html>");
    return res.end();
  }

  if (url === "/users") {
    res.write("<html>");
    res.write("<ul>");
    res.write("<li>User 1</li>");
    res.write("<li>User 2</li>");
    res.write("<li>User 3</li>");
    res.write("</ul>");
    res.write("</html>");
    return res.end();
  }

  if (url === "/create-user" && method === "POST") {
    const body = [];
    req.on("data", chunk => {
      body.push(chunk);
    });
    return req.on("end", () => {
      const parsedUser = Buffer.concat(body).toString();
      const user = parsedUser.split("=")[1];
      console.log(user);
      return res.end();
    });
  }
};

exports.handler = requestHandler;
