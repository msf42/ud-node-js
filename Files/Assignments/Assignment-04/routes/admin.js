const path = require('path');

const express = require('express');

const rootDir = require('../util/path');

const router = express.Router();

const users = [];

// /admins/add-user => GET
router.get('/add-user', (req, res, next) => {
  res.render('add-user', {pageTitle: 'Add User', path: '/admin/add-user', formCSS: true, userCSS: true, activeAddUser: true})
});

// /admin/add-user => POST
router.post('/add-user', (req, res, next) => {
  users.push({ title: req.body.title });
  res.redirect('/');
});

exports.routes = router;
exports.users = users;
